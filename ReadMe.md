# INO Unicode Library
INO is a modern Object Pascal Unicode library for the [Free Pascal](https://www.freepascal.org/) and [Delphi](https://www.embarcadero.com/products/delphi) compilers. 
INO features :
  - An implementation of the [Unicode Collation Algorithm (UCA)](http://www.unicode.org/reports/tr10/tr10-34.html).
  - [CLDR](http://cldr.unicode.org/) collations support.
  - Case-sensitive comparisons, Case-unsensitive comparisons and other customizations.
  - Routines to get Unicode general properties of unicode characters.
  - Fully Object Pascal without external dependencies.

# Installation
Using INO is as simple as getting the sources' path included in your project or compiler default search path.
Get the sources :
```
git clone https://gitlab.com/inoussa12/ino
```

# Getting started
INO provides the **`TUnicodeCollator`** record (located in the `unicodecollator.pas` unit) to do collation-aware strings comparison. It also has methods for collation key's computation and key's comparison for strings. 
Samples projects are included in the `samples/fpc` and `samples/delphi` for Free Pascal and Delphi compiler.

```Delphi
program sample;
uses
  unicodeducet, collation_tr, unicodecollator;
var
  x : TUnicodeCollator;
begin
  if not x.TrySetCollation('ROOT') then begin
    WriteLn('TUnicodeCollator Fail to Initialize');
    Halt(1);
  end;
  WriteLn('------- Using the CLDR default collation -------');
  WriteLn('Default Mode :');
  WriteLn('    x.CompareString(''AZERTY'',''AZERTY'') = ', x.CompareString('AZERTY','AZERTY'));
  WriteLn('    x.CompareString(''AZERTY'',''azerty'') = ', x.CompareString('AZERTY','azerty'));
  WriteLn('    x.CompareString(''AZERTY'',''AZE$R$TY@'') = ', x.CompareString('AZERTY','AZE$R$TY@'));
  WriteLn('IgnoreCase Mode :');
  x.Mode := TCollatorMode.IgnoreCase;
  WriteLn('    x.CompareString(''AZERTY'',''AZERTY'') = ', x.CompareString('AZERTY','AZERTY'));
  WriteLn('    x.CompareString(''AZERTY'',''azerty'') = ', x.CompareString('AZERTY','azerty'));
  x.Mode := TCollatorMode.IgnoreSymbols;
  WriteLn('IgnoreSymbols Mode :');
  WriteLn('    x.CompareString(''AZERTY'',''AZE$R$TY@'') = ', x.CompareString('AZERTY','AZE$R$TY@'));
  WriteLn;
           
  WriteLn('------- Different sort order / Collations -------');
  x.Mode := TCollatorMode.Default;
  WriteLn('    CLDR default collation:');
  WriteLn('        x.CompareString(''i'',''I'') = ', x.CompareString('i','I'));

  if not x.TrySetCollation('tr') then begin
    WriteLn('TUnicodeCollator Fail to Initialize "tr"');
    Halt(1);
  end;
  WriteLn('    "tr" collation:');
  WriteLn('        x.CompareString(''i'',''I'') = ', x.CompareString('i','I'));
end.
```
License
----
INO source code is released under the MIT licence.
Unicode Data that are used are under the Unicode data licence.