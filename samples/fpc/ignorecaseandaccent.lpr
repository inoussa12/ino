{$CODEPAGE UTF8}
program ignorecaseandaccent;
uses unicodeducet, unicodecollator;

procedure halt_error(const AMsg : string; const AErrorCode : Integer = 1);
begin
  WriteLn(AMsg);
  Readln;
  Halt(AErrorCode);
end;

var
  x : TUnicodeCollator;
begin
  if not x.TrySetCollation('ROOT') then
    halt_error('TUnicodeCollator Fail to Initialize');    
  WriteLn('------ Mode: Default  ------');
  x.Mode := TCollatorMode.Default;
  WriteLn('  x.CompareString(''element'',''éLèMênT'') = ',x.CompareString('element','éLèMênT'));
  WriteLn('  x.CompareString(''ELEment'',''élément'') = ',x.CompareString('ELEment','élément'));
  WriteLn('  x.CompareString(''Mais'',''maïs'') = ',x.CompareString('Mais','maïs'));
  WriteLn;

  WriteLn('------ Mode: Ignoring Accent and Case  ------');
  x.Mode := TCollatorMode.IgnoreAccentAndCase;    
  WriteLn('  x.CompareString(''element'',''éLèMênT'') = ',x.CompareString('element','éLèMênT'));
  WriteLn('  x.CompareString(''ELEment'',''élément'') = ',x.CompareString('ELEment','élément'));
  WriteLn('  x.CompareString(''Mais'',''maïs'') = ',x.CompareString('Mais','maïs'));

  ReadLn;
end.
