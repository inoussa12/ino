program sample;
uses
  unicodeducet, collation_tr, unicodecollator;

var
  x : TUnicodeCollator;
begin
  if not x.TrySetCollation('ROOT') then begin
    WriteLn('TUnicodeCollator Fail to Initialize');
    Halt(1);
  end;
  WriteLn('------- Using the CLDR default collation -------');
  WriteLn('Default Mode :');
  WriteLn('    x.CompareString(''AZERTY'',''AZERTY'') = ', x.CompareString('AZERTY','AZERTY'));
  WriteLn('    x.CompareString(''AZERTY'',''azerty'') = ', x.CompareString('AZERTY','azerty'));
  WriteLn('    x.CompareString(''AZERTY'',''AZE$R$TY@'') = ', x.CompareString('AZERTY','AZE$R$TY@'));
  WriteLn('IgnoreCase Mode :');
  x.Mode := TCollatorMode.IgnoreCase;
  WriteLn('    x.CompareString(''AZERTY'',''AZERTY'') = ', x.CompareString('AZERTY','AZERTY'));
  WriteLn('    x.CompareString(''AZERTY'',''azerty'') = ', x.CompareString('AZERTY','azerty'));
  x.Mode := TCollatorMode.IgnoreSymbols;
  WriteLn('IgnoreSymbols Mode :');
  WriteLn('    x.CompareString(''AZERTY'',''AZE$R$TY@'') = ', x.CompareString('AZERTY','AZE$R$TY@'));
  WriteLn;
           
  WriteLn('------- Different sort order / Collations -------');
  x.Mode := TCollatorMode.Default;
  WriteLn('    CLDR default collation:');
  WriteLn('        x.CompareString(''i'',''I'') = ', x.CompareString('i','I'));

  if not x.TrySetCollation('tr') then begin
    WriteLn('TUnicodeCollator Fail to Initialize "tr"');
    Halt(1);
  end;
  WriteLn('    "tr" collation:');
  WriteLn('        x.CompareString(''i'',''I'') = ', x.CompareString('i','I'));

  ReadLn;
end.

