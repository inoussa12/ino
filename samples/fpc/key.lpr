program key;  
uses
  sysutils,
  unicodeducet, collation_tr, unicodecollator;

function ToString(AKey : TCollatorSortKey) : string;
var
  i : Integer;
begin
  Result := '';
  for i := Low(AKey) to High(AKey) do
    Result := Result+IntToHex(AKey[i],4) + ' ';
end;

var
  x : TUnicodeCollator;
  k1, k2 : TCollatorSortKey;
begin
  if not x.TrySetCollation('ROOT') then begin
    WriteLn('TUnicodeCollator Fail to Initialize');
    Halt(1);
  end;
  WriteLn('------- Using the CLDR default collation -------');   
  WriteLn('Default Mode :');
  WriteLn('  x.ComputeSortKey(''Aze'') = ',ToString(x.ComputeSortKey('Aze')));
  WriteLn('  x.ComputeSortKey(''AZE'') = ',ToString(x.ComputeSortKey('AZE')));
  WriteLn('  x.ComputeSortKey(''AZE@'') = ',ToString(x.ComputeSortKey('AZE@')));
  WriteLn('IgnoreCase Mode :');
  x.Mode := TCollatorMode.IgnoreCase;
  WriteLn('  x.ComputeSortKey(''Aze'') = ',ToString(x.ComputeSortKey('Aze')));
  WriteLn('  x.ComputeSortKey(''AZE'') = ',ToString(x.ComputeSortKey('AZE')));
  WriteLn('  x.ComputeSortKey(''AZE@'') = ',ToString(x.ComputeSortKey('AZE@')));
  x.Mode := TCollatorMode.IgnoreSymbols;
  WriteLn('IgnoreSymbols Mode :');
  WriteLn('  x.ComputeSortKey(''Aze'') = ',ToString(x.ComputeSortKey('Aze')));
  WriteLn('  x.ComputeSortKey(''AZE'') = ',ToString(x.ComputeSortKey('AZE')));
  WriteLn('  x.ComputeSortKey(''AZE@'') = ',ToString(x.ComputeSortKey('AZE@')));

  WriteLn('------- Using the CLDR default collation -------');   
  WriteLn('  x.ComputeSortKey(''I'') = ',ToString(x.ComputeSortKey('I')));  
  WriteLn('------- Using the "tr"  collation -------');      
  if not x.TrySetCollation('tr') then begin
    WriteLn('TUnicodeCollator Fail to Initialize');
    Halt(1);
  end;
  k1 := x.ComputeSortKey('I'); // sample Key variable. 
  k2 := x.ComputeSortKey('i'); // sample Key variable.
  WriteLn('  x.ComputeSortKey(''I'') = ',ToString(k1)); 
  WriteLn('  x.ComputeSortKey(''i'') = ',ToString(k2));
  WriteLn(' Comparing Keys *****');
  WriteLn('  x.CompareSortKey(k1,k2) = ',x.CompareSortKey(k1,k2));
  ReadLn;
end.

