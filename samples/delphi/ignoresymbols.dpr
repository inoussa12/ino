﻿program ignoresymbols;

{$APPTYPE CONSOLE}

{$R *.res}

uses unicodeducet, unicodecollator;

procedure halt_error(const AMsg : string; const AErrorCode : Integer = 1);
begin
  WriteLn(AMsg);
  Readln;
  Halt(AErrorCode);
end;

var
  x : TUnicodeCollator;
begin
  if not x.TrySetCollation('ROOT') then
    halt_error('TUnicodeCollator Fail to Initialize');
    WriteLn('------ Mode: Default ------');
    x.Mode := TCollatorMode.Default;
    WriteLn('  x.CompareString(''Word'',''W$o£r¤d'') = ',x.CompareString('Word','W$o£r¤d'));
    WriteLn('  x.CompareString(''®©¥'',''$£¤$£¤$£¤'') = ',x.CompareString('®©¥','$£¤$£¤$£¤'));
    WriteLn('  x.CompareString(''Unicode'',''¤¤¤ Unicode ®©¥¤¤¤'') = ',x.CompareString('Unicode','¤¤¤ Unicode ®©¥¤¤¤'));
    WriteLn;

    WriteLn('------ Mode: Ignoring Symbols ------');
    x.Mode := TCollatorMode.IgnoreSymbols;
    WriteLn('  x.CompareString(''Word'',''W$o£r¤d'') = ',x.CompareString('Word','W$o£r¤d'));
    WriteLn('  x.CompareString(''®©¥'',''$£¤$£¤$£¤'') = ',x.CompareString('®©¥','$£¤$£¤$£¤'));
    WriteLn('  x.CompareString(''Unicode'',''¤¤¤ Unicode ®©¥¤¤¤'') = ',x.CompareString('Unicode','¤¤¤ Unicode ®©¥¤¤¤'));

  ReadLn;
end.
