program ignorecase;

{$APPTYPE CONSOLE}

{$R *.res}

uses
  unicodeducet,
  unicodecollator;

procedure halt_error(const AMsg : string; const AErrorCode : Integer = 1);
begin
  WriteLn(AMsg);
  Readln;
  Halt(AErrorCode);
end;

var
  x : TUnicodeCollator;
begin
  if not x.TrySetCollation('ROOT') then
    halt_error('TUnicodeCollator Fail to Initialize');
  WriteLn('------ Mode: Default ------');
  x.Mode := TCollatorMode.Default;
  WriteLn('  x.CompareString(''AZERTY'',''azerty'') = ',x.CompareString('AZERTY','azerty'));
  WriteLn('  x.CompareString(''AZERTY'',''AzERty'') = ',x.CompareString('AZERTY','AzERty'));
  WriteLn('  x.CompareString(''azerty'',''azerty'') = ',x.CompareString('azerty','azerty'));
  WriteLn('  x.CompareString(''AzERty'',''azerty'') = ',x.CompareString('AzERty','azerty'));
  WriteLn;

  WriteLn('------ Mode: Ignoring Case ------');
  x.Mode := TCollatorMode.IgnoreCase;
  WriteLn('  x.CompareString(''AZERTY'',''azerty'') = ',x.CompareString('AZERTY','azerty'));
  WriteLn('  x.CompareString(''AZERTY'',''AzERty'') = ',x.CompareString('AZERTY','AzERty'));
  WriteLn('  x.CompareString(''azerty'',''azerty'') = ',x.CompareString('azerty','azerty'));
  WriteLn('  x.CompareString(''AzERty'',''azerty'') = ',x.CompareString('AzERty','azerty'));

  ReadLn;
end.

