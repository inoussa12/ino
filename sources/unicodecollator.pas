{ Unicode Collator.

    Copyright (c) 2017 by Inoussa OUEDRAOGO.

    The source code is distributed under the MIT license.
    See the LICENSE file in the project root for more information.
}
unit unicodecollator;
{$IFDEF FPC}
  {$MODE DELPHI}{$H+}
{$ENDIF FPC}

{$SCOPEDENUMS ON}
{$POINTERMATH ON}
{$TYPEDADDRESS ON}

interface

uses
  unicodedata;

type

  TCollatorSortKey = TUCASortKey;
  TCollatorHandle = PUCA_DataBook;

  TCollatorMode = (
    Default, Custom,
    IgnoreAccentAndCase, IgnoreCase, IgnorePunctuation, IgnoreSymbols
  );

  { TUnicodeCollator }

  TUnicodeCollator = record
  private
    type
      TStrenghLevel = (Primary, Secondary, Tertiary);
      TFilterDataRec = record
        Data  : TCategoryMask;
        Valid : Boolean;
      end;
      PFilterDataRec = ^TFilterDataRec;

      TStateDataRec = packed record
        HandleObject : TUCA_DataBook;
        Filter       : TFilterDataRec;
        Mode         : TCollatorMode;
        Ready        : Boolean;
      end;
      PStateDataRec = ^TStateDataRec;
  private
    // FBuffer is used as the store because :
    //   1) SizeOf(TUnicodeCollatorB) => SizeOf(Pointer)
    //   2) It is auto-managed by the compiler
    FBuffer : array of Byte;
  private
    function GetMode : TCollatorMode;
    function HasState() : Boolean;
    procedure PrepareBuffer();overload;inline;
    procedure PrepareBuffer(const ASize : Integer);overload;
    function GetState() : PStateDataRec;
    function GetHandle() : TCollatorHandle;
    function HasFilterData() : Boolean;inline;
    function GetFilterData() : PFilterDataRec;inline;
    procedure SetFilterMask(const AMask : TCategoryMask);
    procedure SetMode(AValue : TCollatorMode);
    function TrySetCollation(
            AHandle      : TCollatorHandle;
      const AClearBefore : Boolean
    ) : Boolean;overload;inline;

    property State : PStateDataRec read GetState;
  public
    function IsValid() : Boolean;
    procedure CheckValid();
    procedure Clear();
    function TrySetCollation(AHandle : TCollatorHandle) : Boolean;overload;
    function TrySetCollation(ABaseLocale : UnicodeString) : Boolean;overload;inline;
    function ComputeSortKey(
      AStr    : PUnicodeChar;
      ALength : SizeInt
    ) : TCollatorSortKey;overload;
    function ComputeSortKey(const AStr : UnicodeString) : TCollatorSortKey;overload;inline;
    function CompareSortKey(const A, B : TCollatorSortKey) : Integer;overload;inline;
    function CompareString(
      AStrA      : PUnicodeChar;
      ALengthA   : SizeInt;
      AStrB      : PUnicodeChar;
      ALengthB   : SizeInt
    ) : Integer;overload;
    function CompareString(
      const AStrA,
            AStrB : UnicodeString
    ) : Integer;overload;inline;

    procedure Assign(ASource : TUnicodeCollator);
    function Clone() : TUnicodeCollator;
    property Handle : TCollatorHandle read GetHandle;
    property Mode : TCollatorMode read GetMode write SetMode;
  end;

implementation

function ValueOfLevel(const ALevel : TUnicodeCollator.TStrenghLevel) : Byte;inline;
begin
  Result := Ord(ALevel)+1;
end;

{ TUnicodeCollator }

function TUnicodeCollator.GetMode : TCollatorMode;
begin
  Result := State^.Mode;
end;

function TUnicodeCollator.HasState() : Boolean;
begin
  Result := (Length(FBuffer) >= SizeOf(TStateDataRec));
end;

procedure TUnicodeCollator.PrepareBuffer;
begin
  PrepareBuffer(SizeOf(TStateDataRec));
end;

procedure TUnicodeCollator.PrepareBuffer(const ASize : Integer);
begin
  if (Length(FBuffer) < ASize) then
    SetLength(FBuffer,ASize);
end;

function TUnicodeCollator.GetState() : PStateDataRec;
begin
  if (FBuffer = nil) then
    PrepareBuffer();
  Result := PStateDataRec(@FBuffer[Low(FBuffer)]);
end;

function TUnicodeCollator.GetHandle() : TCollatorHandle;
begin
  Result := @State^.HandleObject;
end;

function TUnicodeCollator.HasFilterData() : Boolean;
begin
  Result := State^.Filter.Valid;
end;

function TUnicodeCollator.GetFilterData() : PFilterDataRec;
begin
  Result := @State^.Filter;
end;

procedure TUnicodeCollator.SetFilterMask(const AMask : TCategoryMask);
var
  fd : PFilterDataRec;
begin
  fd := GetFilterData();
  fd^.Data := AMask;
  fd^.Valid := (fd^.Data <> []);
end;

procedure TUnicodeCollator.SetMode(AValue : TCollatorMode);

  procedure HandleDefault();
  var
    s : UnicodeString;
  begin
    s := BytesToName(Handle^.CollationName);
    Clear();
    TrySetCollation(s);
  end;

begin
  case AValue of
    TCollatorMode.Default :
      begin
        HandleDefault();
        State^.Mode := AValue;
      end;
    TCollatorMode.IgnoreAccentAndCase :
      begin
        Handle^.ComparisonStrength := ValueOfLevel(TStrenghLevel.Primary);
        State^.Mode := AValue;
      end;
    TCollatorMode.IgnoreCase :
      begin
        Handle^.ComparisonStrength := ValueOfLevel(TStrenghLevel.Secondary);
        State^.Mode := AValue;
      end;
    TCollatorMode.IgnorePunctuation :
      begin
        Handle^.ComparisonStrength := ValueOfLevel(TStrenghLevel.Tertiary);
        Handle^.VariableWeight := TUCA_VariableKind.ucaShifted;
        State^.Mode := AValue;
      end;
    TCollatorMode.IgnoreSymbols :
      begin
        SetFilterMask(
          [UGC_MathSymbol,UGC_CurrencySymbol,UGC_ModifierSymbol,UGC_OtherSymbol]
        ); 
        HandleDefault();
        State^.Mode := AValue;
      end;
  end;
end;

function TUnicodeCollator.TrySetCollation(
        AHandle      : TCollatorHandle;
  const AClearBefore : Boolean
) : Boolean;
begin
  if AClearBefore then
    Clear();
  Result := (AHandle <> nil);
  if Result then begin
    Handle^ := AHandle^;
    State^.Ready := True;
  end;
end;

function TUnicodeCollator.IsValid() : Boolean;
begin
  Result := HasState() and State^.Ready;
end;

procedure TUnicodeCollator.CheckValid();
begin
  if not IsValid() then
    Error(reInvalidOp);
end;

procedure TUnicodeCollator.Clear();
begin
  if (Length(FBuffer) > 0) then
    FillChar(FBuffer[Low(FBuffer)],Length(FBuffer),0);
end;

function TUnicodeCollator.TrySetCollation(AHandle : TCollatorHandle) : Boolean;
begin
  Result := TrySetCollation(AHandle,True);
end;

function TUnicodeCollator.TrySetCollation(ABaseLocale : UnicodeString) : Boolean;
begin
  Clear();
  Result := (ABaseLocale <> '') and TrySetCollation(FindCollation(ABaseLocale));
end;

function TUnicodeCollator.ComputeSortKey(
  AStr    : PUnicodeChar;
  ALength : SizeInt
) : TCollatorSortKey;

  function FilterAndCompute() : TCollatorSortKey;
  var
    fd : PFilterDataRec;
    s : UnicodeString;
  begin
    fd := GetFilterData();
    if fd^.Valid and (fd^.Data <> []) then begin
      s := FilterString(AStr,ALength,fd^.Data);
      Result := unicodedata.ComputeSortKey(s,Handle);
    end else begin
      Result := unicodedata.ComputeSortKey(AStr,ALength,Handle);
    end;
  end;

begin
  CheckValid();
  if HasFilterData() then
    Result := FilterAndCompute()
  else
    Result := unicodedata.ComputeSortKey(AStr,ALength,Handle);
end;

function TUnicodeCollator.ComputeSortKey(const AStr : UnicodeString) : TCollatorSortKey;
begin
  Result := ComputeSortKey(@AStr[1],Length(AStr));
end;

function TUnicodeCollator.CompareSortKey(const A, B : TCollatorSortKey) : Integer;
begin
  Result := unicodedata.CompareSortKey(A,B);
end;

function TUnicodeCollator.CompareString(
  AStrA      : PUnicodeChar;
  ALengthA   : SizeInt;
  AStrB      : PUnicodeChar;
  ALengthB   : SizeInt
) : Integer;

  function FilterAndCompare() : Integer;
  var
    fd : PFilterDataRec;
    a, b : UnicodeString;
  begin
    fd := GetFilterData();
    if fd^.Valid and (fd^.Data <> []) then begin
      a := FilterString(AStrA,ALengthA,fd^.Data);
      b := FilterString(AStrB,ALengthB,fd^.Data);
      Result := unicodedata.IncrementalCompareString(a,b,Handle);
    end else begin
      Result := unicodedata.IncrementalCompareString(AStrA,ALengthA,AStrB,ALengthB,Handle);
    end;
  end;

begin
  CheckValid();
  if HasFilterData() then
    Result := FilterAndCompare()
  else
    Result := unicodedata.IncrementalCompareString(AStrA,ALengthA,AStrB,ALengthB,Handle);
end;

function TUnicodeCollator.CompareString(const AStrA, AStrB : UnicodeString) : Integer;
begin
  Result := CompareString(@AStrA[1],Length(AStrA),@AStrB[1],Length(AStrB));
end;

procedure TUnicodeCollator.Assign(ASource : TUnicodeCollator);
begin
  Self.FBuffer := Copy(ASource.FBuffer);
end;

function TUnicodeCollator.Clone() : TUnicodeCollator;
begin
  Result.Assign(Self);
end;

end.

